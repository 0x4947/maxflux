#!/bin/bash

latest_tag=$(git describe --tags --abbrev=0)
if [[ "$latest_tag" =~ ^v([0-9]+)\.([0-9]+)\.([0-9]+)$ ]];
then
	latest_major_digit=${BASH_REMATCH[1]}
	latest_minor_digit=${BASH_REMATCH[2]}
	latest_patch_digit=${BASH_REMATCH[3]}
else
	>&2 echo "Failed to parse version from the latest tag. Tag must follow Semantic Versioning 1.0.0." 
	exit 1
fi

if [ "${MAXFLUX_RELEASE_TYPE^^}" = "MAJOR" ];
then
	next_version="$((latest_major_digit + 1)).0.0"
elif [ "${MAXFLUX_RELEASE_TYPE^^}" = "MINOR" ];
then
	next_version="$latest_major_digit.$((latest_minor_digit + 1)).0"
elif [ "${MAXFLUX_RELEASE_TYPE^^}" = "PATCH" ];
then
	next_version="$latest_major_digit.$latest_minor_digit.$((latest_patch_digit + 1))"
else
	>&2 echo "Missing/invalid MAXFLUX_RELEASE_TYPE parameter."
	exit 1
fi

echo $next_version
