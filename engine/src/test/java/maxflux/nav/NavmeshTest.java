package maxflux.nav;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.fail;

import org.junit.Test;

import maxflux.math.Point;

public class NavmeshTest {

	@Test
	public void testNavnodeFullySurroundedByNavtriangles() {
		var anode = new Navnode(new Point(1, 4));
		var bnode = new Navnode(new Point(5, 4));
		var cnode = new Navnode(new Point(3, 3));
		var dnode = new Navnode(new Point(3, 2));
		var navmesh = new Navmesh(new Navtriangle(anode, bnode, cnode));
		navmesh.add(new Navtriangle(bnode, cnode, dnode));
		try {
			navmesh.add(new Navtriangle(anode, cnode, dnode));
			fail("Must not allow to have a fully surrounded navnode.");
		}catch(IllegalArgumentException e) {
			assertThat(e.getMessage()).isEqualTo("Navnode [point=Point[x=3.0, y=3.0]] is fully surrounded by navtriangles. Each navnode must represent the constrainted edge.");
		}
	}

}
