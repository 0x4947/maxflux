package maxflux.nav;

import static maxflux.assertion.CustomAssertions.assertThat;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;

import maxflux.Unit;
import maxflux.World;
import maxflux.command.AddUnitCommand;
import maxflux.command.MoveUnitCommand;
import maxflux.math.Point;

public class PathfindingTest {
	@Rule
	public Timeout timeout = new Timeout(10000);
	
	@Test
	public void testWithinSameNavtriangle() {
		var anode = new Navnode(new Point(-1.5, 1.5));
		var bnode = new Navnode(new Point(0, 1.5));
		var cnode = new Navnode(new Point(0, 0));
		var dnode = new Navnode(new Point(1.5, 1.5));
		var enode = new Navnode(new Point(1.5, -1.5));
		var fnode = new Navnode(new Point(-1.5, -1.5));

		var navtriangle = new Navtriangle(anode, bnode, cnode);
		var navmesh = new Navmesh(navtriangle);
		navmesh.add(new Navtriangle(bnode, cnode, dnode));
		navmesh.add(new Navtriangle(cnode, dnode, enode));
		navmesh.add(new Navtriangle(cnode, enode, fnode));

		var pathfinder = new AstarSmoothPathfinder(navmesh);
		var unit = new Unit(new Point(-0.5, 1), 1, pathfinder);
		var world = new World();
		world.queueUpCommand(new AddUnitCommand(unit));
		world.queueUpCommand(new MoveUnitCommand(unit, new Point(-1, 1.25)));
		world.run(280);
		assertThat(unit.getPosition()).doesIntersect(new Point(-0.7504, 1.1252));
		world.run(280);
		assertThat(unit.getPosition()).doesIntersect(new Point(-1, 1.25));
		world.run(1000);
		assertThat(unit.getPosition()).doesIntersect(new Point(-1, 1.25));
	}

	@Test
	public void testAroundCorner() {
		var anode = new Navnode(new Point(-1.5, 1.5));
		var bnode = new Navnode(new Point(0, 1.5));
		var cnode = new Navnode(new Point(0, 0));
		var dnode = new Navnode(new Point(1.5, 1.5));
		var enode = new Navnode(new Point(1.5, -1.5));
		var fnode = new Navnode(new Point(-1.5, -1.5));

		var navtriangle = new Navtriangle(anode, bnode, cnode);
		var navmesh = new Navmesh(navtriangle);
		navmesh.add(new Navtriangle(bnode, cnode, dnode));
		navmesh.add(new Navtriangle(cnode, dnode, enode));
		navmesh.add(new Navtriangle(cnode, enode, fnode));

		var pathfinder = new AstarSmoothPathfinder(navmesh);
		var unit = new Unit(new Point(-0.5, 1), 1, pathfinder);
		var world = new World();
		world.queueUpCommand(new AddUnitCommand(unit));
		world.queueUpCommand(new MoveUnitCommand(unit, new Point(-0.5, -1)));
		world.run(559);
		assertThat(unit.getPosition()).doesIntersect(new Point(-0.25, 0.5));
		world.run(559);
		assertThat(unit.getPosition()).doesIntersect(new Point(0, 0));
		world.run(559);
		assertThat(unit.getPosition()).doesIntersect(new Point(-0.25, -0.5));
		world.run(1000);
		assertThat(unit.getPosition()).doesIntersect(new Point(-0.5, -1));
	}

	@Test
	public void testAroundCorners() {
		var anode = new Navnode(new Point(-1.5, 1.5));
		var bnode = new Navnode(new Point(1.5, 1.5));
		var cnode = new Navnode(new Point(0, 0));
		var dnode = new Navnode(new Point(1.5, -4));
		var enode = new Navnode(new Point(0, -4));
		var fnode = new Navnode(new Point(1.5, -7));
		var gnode = new Navnode(new Point(-10, -8));

		var navtriangle = new Navtriangle(anode, bnode, cnode);
		var navmesh = new Navmesh(navtriangle);
		navmesh.add(new Navtriangle(cnode, bnode, dnode));
		navmesh.add(new Navtriangle(cnode, dnode, enode));
		navmesh.add(new Navtriangle(dnode, enode, fnode));
		navmesh.add(new Navtriangle(enode, fnode, gnode));

		var pathfinder = new AstarSmoothPathfinder(navmesh);
		var unit = new Unit(new Point(-0.5, 1), 1, pathfinder);
		var world = new World();
		world.queueUpCommand(new AddUnitCommand(unit));
		world.queueUpCommand(new MoveUnitCommand(unit, new Point(-2, -6)));
		world.run(1118);
		assertThat(unit.getPosition()).doesIntersect(new Point(0, 0));
		world.run(500);
		assertThat(unit.getPosition()).doesIntersect(new Point(0, -0.5));
		world.run(3000);
		assertThat(unit.getPosition()).doesIntersect(new Point(0, -3.5));
		world.run(500 + 707);
		assertThat(unit.getPosition()).doesIntersect(new Point(-0.5, -4.5));
		world.run(5000);
		assertThat(unit.getPosition()).doesIntersect(new Point(-2, -6));
	}

	@Test
	public void testFindingShortestRouteNoSmoothingNeeded() {
		var world = new World();

		var anode = new Navnode(new Point(10, 10));
		var bnode = new Navnode(new Point(10, 14));
		var cnode = new Navnode(new Point(20, 10));
		var dnode = new Navnode(new Point(20, 14));
		var enode = new Navnode(new Point(24, 0));
		var fnode = new Navnode(new Point(24, 14));
		var gnode = new Navnode(new Point(11, 4));
		var hnode = new Navnode(new Point(7, 0));
		var inode = new Navnode(new Point(10, 4));
		var jnode = new Navnode(new Point(7, 13));

		var navtriangle = new Navtriangle(anode, bnode, cnode);
		var navmesh = new Navmesh(navtriangle);
		navmesh.add(new Navtriangle(bnode, cnode, dnode));
		navmesh.add(new Navtriangle(cnode, dnode, enode));
		navmesh.add(new Navtriangle(dnode, enode, fnode));
		navmesh.add(new Navtriangle(cnode, gnode, enode));
		navmesh.add(new Navtriangle(gnode, hnode, enode));
		navmesh.add(new Navtriangle(inode, hnode, gnode));
		navmesh.add(new Navtriangle(anode, hnode, inode));
		navmesh.add(new Navtriangle(jnode, hnode, anode));
		navmesh.add(new Navtriangle(jnode, anode, bnode));

		var pathfinder = new AstarSmoothPathfinder(navmesh);
		var unit1 = new Unit(new Point(13, 11), 1, pathfinder);
		world.queueUpCommand(new AddUnitCommand(unit1));
		world.queueUpCommand(new MoveUnitCommand(unit1, new Point(15, 5)));
		world.run(7071);
		assertThat(unit1.getPosition()).doesIntersect(new Point(20, 10));
		world.run(2828);
		assertThat(unit1.getPosition()).doesIntersect(new Point(18.0003, 8.0003));
		world.run(5000);
		assertThat(unit1.getPosition()).doesIntersect(new Point(15, 5));

		// slightly different destination should result in a completely different route
		var unit2 = new Unit(new Point(12, 11), 1, pathfinder);
		world.queueUpCommand(new AddUnitCommand(unit2));
		world.queueUpCommand(new MoveUnitCommand(unit2, new Point(15, 4.5)));
		world.run(2236 + 1000);
		assertThat(unit2.getPosition()).doesIntersect(new Point(10, 9));
		world.run(5000);
		assertThat(unit2.getPosition()).doesIntersect(new Point(10, 4));
		world.run(500);
		assertThat(unit2.getPosition()).doesIntersect(new Point(10.5, 4));
		world.run(2516);
		assertThat(unit2.getPosition()).doesIntersect(new Point(13.0003, 4.25));
	}

	@Test
	public void testBasicFunnelingToNeighborNavtriangle() {
		var anode = new Navnode(new Point(1.02, 2.61));
		var bnode = new Navnode(new Point(3, 3));
		var cnode = new Navnode(new Point(2, 0.43));
		var dnode = new Navnode(new Point(4.98, 1.75));

		var navmesh = new Navmesh(new Navtriangle(anode, bnode, cnode));
		navmesh.add(new Navtriangle(anode, cnode, dnode));

		var pathfinder = new AstarSmoothPathfinder(navmesh);
		var unit = new Unit(new Point(1.4, 2.41), 1, pathfinder);
		var world = new World();
		world.queueUpCommand(new AddUnitCommand(unit));
		world.queueUpCommand(new MoveUnitCommand(unit, new Point(3.6, 2.01)));
		world.run(1000);
		assertThat(unit.getPosition()).doesIntersect(new Point(2.38387, 2.231115));
		world.run(1000);
		assertThat(unit.getPosition()).doesIntersect(new Point(3.36773, 2.05223));
		world.run(1000);
		assertThat(unit.getPosition()).doesIntersect(new Point(3.6, 2.01));
	}

	@Test
	public void testFindingShortestRouteSmoothingNeeded() {
		var anode = new Navnode(new Point(1200, 700));
		var bnode = new Navnode(new Point(1200, 600));
		var cnode = new Navnode(new Point(1000, 600));
		var dnode = new Navnode(new Point(1000, 700));
		var enode = new Navnode(new Point(900, 600));
		var fnode = new Navnode(new Point(900, 700));
		var gnode = new Navnode(new Point(500, 600));
		var hnode = new Navnode(new Point(500, 700));
		var inode = new Navnode(new Point(1000, 300));
		var jnode = new Navnode(new Point(900, 300));
		var knode = new Navnode(new Point(800, 550));
		var lnode = new Navnode(new Point(1100, 450));
		var mnode = new Navnode(new Point(700, 300));
		var nnode = new Navnode(new Point(700, 200));
		var onode = new Navnode(new Point(900, 200));
		var pnode = new Navnode(new Point(1000, 200));
		var qnode = new Navnode(new Point(800, 400));
		var rnode = new Navnode(new Point(400, 600));
		var snode = new Navnode(new Point(650, 250));
		var tnode = new Navnode(new Point(1250, 680));
		var unode = new Navnode(new Point(1175, 800));
		var navmesh = new Navmesh(new Navtriangle(anode, bnode, cnode));
		navmesh.add(new Navtriangle(anode, cnode, dnode));
		navmesh.add(new Navtriangle(cnode, dnode, fnode));
		navmesh.add(new Navtriangle(cnode, fnode, enode));
		navmesh.add(new Navtriangle(fnode, enode, gnode));
		navmesh.add(new Navtriangle(fnode, gnode, hnode));
		navmesh.add(new Navtriangle(enode, cnode, inode));
		navmesh.add(new Navtriangle(enode, jnode, inode));
		navmesh.add(new Navtriangle(enode, knode, jnode));
		navmesh.add(new Navtriangle(cnode, lnode, inode));
		navmesh.add(new Navtriangle(jnode, inode, onode));
		navmesh.add(new Navtriangle(pnode, inode, onode));
		navmesh.add(new Navtriangle(jnode, nnode, onode));
		navmesh.add(new Navtriangle(jnode, nnode, mnode));
		navmesh.add(new Navtriangle(qnode, knode, jnode));
		navmesh.add(new Navtriangle(rnode, gnode, hnode));
		navmesh.add(new Navtriangle(snode, mnode, nnode));
		navmesh.add(new Navtriangle(tnode, anode, bnode));
		navmesh.add(new Navtriangle(tnode, anode, unode));
		var pathfinder = new AstarSmoothPathfinder(navmesh);

		// Moving to directly visible dest through the lengthy corridor
		var unit = new Unit(new Point(1060, 660), 50, pathfinder);
		var world = new World();
		world.queueUpCommand(new AddUnitCommand(unit));
		world.queueUpCommand(new MoveUnitCommand(unit, new Point(450, 610)));
		world.run(1000);
		assertThat(unit.getPosition()).doesIntersect(new Point(1010.1671, 655.91525));
		world.run(1000);
		assertThat(unit.getPosition()).doesIntersect(new Point(960.33424, 651.83067));
		world.run(1000);
		assertThat(unit.getPosition()).doesIntersect(new Point(910.50137, 647.74601));
		world.run(1000);
		assertThat(unit.getPosition()).doesIntersect(new Point(860.66849, 643.66135));
		world.run(7000);
		assertThat(unit.getPosition()).doesIntersect(new Point(511.83836, 615.06871));
		world.run(1000);
		assertThat(unit.getPosition()).doesIntersect(new Point(462.00548, 610.98405));
		world.run(1000);
		assertThat(unit.getPosition()).doesIntersect(new Point(450, 610));

		// Moving to dest through a set of corridors
		var unit2 = new Unit(new Point(1060, 660), 50, pathfinder);
		world.queueUpCommand(new AddUnitCommand(unit2));
		world.queueUpCommand(new MoveUnitCommand(unit2, new Point(690, 215)));
		world.run(1697);
		assertThat(unit2.getPosition()).doesIntersect(new Point(1000.00198, 600.00198));
		world.run(3162);
		assertThat(unit2.getPosition()).doesIntersect(new Point(950.00527, 450.01583));
		world.run(3162 + 2265); // went around the corner
		assertThat(unit2.getPosition()).doesIntersect(new Point(795.05161, 257.52089));
		world.run(10000);
		assertThat(unit2.getPosition()).doesIntersect(new Point(690, 215));
		
		// Left funnel side contracts down to origin and never goes over expanding right side.
		var unit3 = new Unit(new Point(990, 250), 50, pathfinder);
		world.queueUpCommand(new AddUnitCommand(unit3));
		world.queueUpCommand(new MoveUnitCommand(unit3, new Point(890, 450)));
		world.run(1000);
		assertThat(unit3.getPosition()).doesIntersect(new Point(967.6394, 294.7214));
		world.run(1000);
		assertThat(unit3.getPosition()).doesIntersect(new Point(945.27864, 339.44271));
		world.run(1000);
		assertThat(unit3.getPosition()).doesIntersect(new Point(922.91796, 384.16407));
		world.run(1500);
		assertThat(unit3.getPosition()).doesIntersect(new Point(890, 450));
		
		// Right funnel side contracts down to origin and never goes over expanding left side.
		var unit4 = new Unit(new Point(1060, 660), 50, pathfinder);
		world.queueUpCommand(new AddUnitCommand(unit4));
		world.queueUpCommand(new MoveUnitCommand(unit4, new Point(850, 525)));
		world.run(1000);
		assertThat(unit4.getPosition()).doesIntersect(new Point(1017.94107, 632.96212));
		world.run(1000);
		assertThat(unit4.getPosition()).doesIntersect(new Point(975.88215, 605.92424));
		world.run(1000);
		assertThat(unit4.getPosition()).doesIntersect(new Point(933.82322, 578.88636));
		world.run(1000);
		assertThat(unit4.getPosition()).doesIntersect(new Point(891.7643, 551.84848));
		world.run(1000);
		assertThat(unit4.getPosition()).doesIntersect(new Point(850, 525));
		
		// Both funnel sides expand and never go over each other.
		var unit5 = new Unit(new Point(1195, 750), 50, pathfinder);
		world.queueUpCommand(new AddUnitCommand(unit5));
		world.queueUpCommand(new MoveUnitCommand(unit5, new Point(875, 575)));
		world.run(1000);
		assertThat(unit5.getPosition()).doesIntersect(new Point(1199.97519, 700.2481));
		world.run(1000);
		assertThat(unit5.getPosition()).doesIntersect(new Point(1153.56547, 682.14056));
		world.run(4000);
		assertThat(unit5.getPosition()).doesIntersect(new Point(966.89635, 610.34475));
		world.run(2000);
		assertThat(unit5.getPosition()).doesIntersect(new Point(875, 575));
	}
}
