package maxflux;

import static org.assertj.core.api.Assertions.assertThat;
import static maxflux.assertion.CustomAssertions.assertThat;

import org.junit.Test;

import maxflux.command.AddUnitCommand;
import maxflux.command.MoveUnitCommand;
import maxflux.math.Point;
import maxflux.nav.DirectPathfinder;

public class WorldTest {

	@Test
	public void testSingleUnitMovingToSingleWaypoint() {
		var origin = new Point(-1, 1);
		var speed = 1.41421;
		var unit = new Unit(origin, speed, new DirectPathfinder());
		var world = new World();

		world.queueUpCommand(new AddUnitCommand(unit));
		world.run(1000);
		assertThat(unit.getPosition()).isEqualTo(origin);

		world.queueUpCommand(new MoveUnitCommand(unit, new Point(4.5, -4.5)));
		world.run(1000);
		assertThat(unit.getPosition()).doesIntersect(new Point(0, 0));

		world.run(4000);
		assertThat(unit.getPosition()).doesIntersect(new Point(4, -4));

		world.queueUpCommand(new MoveUnitCommand(unit, new Point(5.5, -2.5)));
		world.run(100);
		assertThat(unit.getPosition()).doesIntersect(new Point(4.1, -3.9));

		world.run(900);
		assertThat(unit.getPosition()).doesIntersect(new Point(5, -3));

		world.run(1000);
		assertThat(unit.getPosition()).doesIntersect(new Point(5.5, -2.5));
	}

}
