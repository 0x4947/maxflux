package maxflux.assertion;

import org.assertj.core.api.AbstractAssert;

import maxflux.math.MathUtils;
import maxflux.math.Vector;

public class VectorAssert extends AbstractAssert<VectorAssert, Vector> {
	public VectorAssert(Vector actual) {
		super(actual, VectorAssert.class);
	}

	public VectorAssert isFuzzyEqualTo(Vector expected) {
		if(!MathUtils.fuzzyEquals(expected.x, actual.x) || !MathUtils.fuzzyEquals(expected.y, actual.y)) {
			failWithMessage("%s is not equal to nor within a margin of error from expected %s.", actual, expected);
		}

		return this;
	}
}
