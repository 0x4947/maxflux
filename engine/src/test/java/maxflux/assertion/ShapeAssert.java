package maxflux.assertion;

import org.assertj.core.api.AbstractAssert;

import maxflux.math.Shape;

public class ShapeAssert extends AbstractAssert<ShapeAssert, Shape> {
	public ShapeAssert(Shape actual) {
		super(actual, ShapeAssert.class);
	}

	public ShapeAssert doesIntersect(Shape shape) {
		isNotNull();

		if(!actual.intersects(shape)) {
			failWithMessage("Expected %s to intersect %s!", actual, shape);
		}

		if(!shape.intersects(actual)) {
			failWithMessage("%s intersects %s, but not the other way around!", actual, shape);
		}

		return this;
	}

	public ShapeAssert doesNotIntersect(Shape shape) {
		isNotNull();

		if(actual.intersects(shape)) {
			failWithMessage("Expected %s to not intersect %s!", actual, shape);
		}

		if(shape.intersects(actual)) {
			failWithMessage("%s does not intersect %s, but not the other way around!", shape, actual);
		}

		return this;
	}
}
