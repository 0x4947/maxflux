package maxflux.assertion;

import maxflux.math.Shape;
import maxflux.math.Vector;

public final class CustomAssertions {
	private CustomAssertions() {
	}

	public static ShapeAssert assertThat(Shape shape) {
		return new ShapeAssert(shape);
	}

	public static VectorAssert assertThat(Vector vector) {
		return new VectorAssert(vector);
	}
}
