package maxflux.math;

import static maxflux.assertion.CustomAssertions.assertThat;

import org.junit.Test;

public class TriangleTest {

	@Test
	public void testTrianglePointIntersection() {
		var p1 = new Point(-2, 2);
		var p2 = new Point(-0.5, -1.5);
		var p3 = new Point(-1, 2.5);
		var t = new Triangle(p1, p2, p3);
		assertThat(t).doesNotIntersect(new Point(1, 3));
		assertThat(t).doesNotIntersect(new Point(-1.6, 2.4));
		assertThat(t).doesNotIntersect(new Point(-2, 0));
		assertThat(t).doesIntersect(new Point(-1, 0.5));
		assertThat(t).doesIntersect(new Point(-1.8, 2));
		assertThat(t).doesIntersect(new Point(-0.8, -0.5));
		assertThat(t).doesIntersect(new Point(-1.17893, 0.08417));
		assertThat(t).doesNotIntersect(new Point(-1.17896, 0.0841));
		assertThat(t).doesIntersect(new Point(-0.50001, -1.49996));
		assertThat(t).doesIntersect(new Point(-2, 2));
		assertThat(t).doesIntersect(new Point(-0.5, -1.5));
		assertThat(t).doesIntersect(new Point(-1, 2.5));
	}

}
