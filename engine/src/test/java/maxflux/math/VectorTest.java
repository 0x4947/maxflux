package maxflux.math;

import static maxflux.assertion.CustomAssertions.assertThat;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.within;

import org.assertj.core.data.Offset;
import org.junit.Test;

public class VectorTest {

	@Test
	public void testCreationFromTwoPoints() {
		var p1 = new Point(5, 3);
		var p2 = new Point(0, -1);
		assertThat(new Vector(p1, p2)).isEqualTo(new Vector(-5, -4));
	}

	@Test
	public void testMagnitude() {
		assertThat(new Vector(1, 0).getMagnitude()).isEqualTo(1);
		assertThat(new Vector(-1, 1.5).getMagnitude()).isCloseTo(1.8027756377319946, Offset.offset(0.00001));
		assertThat(new Vector(0.5, -2).getMagnitude()).isCloseTo(2.0615528128088303, Offset.offset(0.00001));
	}

	@Test
	public void testRadians() {
		assertThat(new Vector(1, 1).getRads()).isCloseTo(0.785398, Offset.offset(0.00001));
		assertThat(new Vector(0, 0).getRads()).isZero();
		assertThat(new Vector(5, 0).getRads()).isZero();
		assertThat(new Vector(-100, 0).getRads()).isCloseTo(Math.PI, Offset.offset(0.00001));
		assertThat(new Vector(-50, -50).getRads()).isCloseTo(3.92699, Offset.offset(0.00001));
		assertThat(new Vector(5, -5).getRads()).isCloseTo(5.49779, Offset.offset(0.00001));
	}

	@Test
	public void testDotProduct() {
		assertThat(new Vector(2, 2).dot(new Vector(-2, 2))).isEqualTo(0);
		assertThat(new Vector(5, 0).dot(new Vector(2.5, 2.5))).isEqualTo(12.5);
		assertThat(new Vector(-1, 1.5).dot(new Vector(0.5, -2))).isEqualTo(-3.5);
	}
	
	@Test
	public void testCrossProduct() {
		assertThat(new Vector(-1.4, 0.6).cross(new Vector(0.6, 1.6))).isCloseTo(-2.6, within(0.00001));
		assertThat(new Vector(0.6, 1.6).cross(new Vector(-1.4, 0.6))).isCloseTo(2.6, within(0.00001));
		assertThat(new Vector(0.5, -0.2).cross(new Vector(-0.9, 0.1))).isCloseTo(-0.13, within(0.00001));
		assertThat(new Vector(0.5, 0.1).cross(new Vector(-0.9, 0.1))).isCloseTo(0.14, within(0.00001));
		assertThat(new Vector(3, 3).cross(new Vector(-2, -2))).isEqualTo(0);
		assertThat(new Vector(10, 7).cross(new Vector(10, 7))).isEqualTo(0);
	}

	@Test
	public void testAngleBetweenTwoVectors() {
		assertThat(new Vector(-1, 1.5).radsBetween(new Vector(0.5, -2))).isCloseTo(2.7985687131621475032, Offset.offset(0.00001));
		assertThat(new Vector(-2, 2).radsBetween(new Vector(2, 2))).isCloseTo(1.5708, Offset.offset(0.00001));
	}

	@Test
	public void testUnitVectorResolution() {
		assertThat(new Vector(3, 4).unit()).isFuzzyEqualTo(new Vector(0.6, 0.8));
		assertThat(new Vector(0.1, -0.1).unit()).isFuzzyEqualTo(new Vector(0.707106781223, -0.707106781223));
	}

}
