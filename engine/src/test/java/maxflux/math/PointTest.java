package maxflux.math;

import static maxflux.assertion.CustomAssertions.assertThat;

import org.junit.Test;

public class PointTest {

	@Test
	public void testIntersectionWithAnotherPoint() {
		assertThat(new Point(5, 3)).doesNotIntersect(new Point(4, 4));
		assertThat(new Point(50, -30)).doesNotIntersect(new Point(-50, -30));
		assertThat(new Point(-123, 17)).doesNotIntersect(new Point(-123, -17));
		assertThat(new Point(28.9876, 5)).doesNotIntersect(new Point(28.98771, 5));
		assertThat(new Point(28, 5.1234)).doesNotIntersect(new Point(28, 5.12329));
		assertThat(new Point(16, -3)).doesIntersect(new Point(16, -3));
		assertThat(new Point(28.9876, 5)).doesIntersect(new Point(28.98769, 5));
		assertThat(new Point(28, 5.1234)).doesIntersect(new Point(28, 5.12331));
	}

}
