package maxflux.math;

import maxflux.math.int32.Inttriangle;

public record Triangle(Point p1, Point p2, Point p3) implements Shape {
	@Override
	public boolean intersects(Shape shape) {
		return switch(shape) {
			case Point p -> {
				var v1 = new Vector(p, p1);
				var v2 = new Vector(p, p2);
				var v3 = new Vector(p, p3);
				var rad1 = v1.radsBetween(v2);
				var rad2 = v2.radsBetween(v3);
				var rad3 = v3.radsBetween(v1);
				var radiansSum = rad1 + rad2 + rad3;
				if(MathUtils.fuzzyEquals(radiansSum, MathUtils.PI2)) {
					yield true;
				}

				yield p.intersects(p1) || p.intersects(p2) || p.intersects(p3);
			}
			case Triangle t -> throw new IllegalArgumentException("Unexpected value: " + shape);
		};
	}

	@Override
	public Inttriangle round() {
		return new Inttriangle(p1.round(), p2.round(), p3.round());
	}
}
