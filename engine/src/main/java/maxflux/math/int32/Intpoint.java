package maxflux.math.int32;

public record Intpoint(int x, int y) implements Intshape {

}
