package maxflux.math.int32;

public record Inttriangle(Intpoint p1, Intpoint p2, Intpoint p3) implements Intshape {

}
