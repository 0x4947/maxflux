package maxflux.math;

import java.util.Objects;

public class Vector {
	public final double x;
	public final double y;
	private Double radians;
	private Double magnitude;

	public Vector(double x, double y) {
		this.x = x;
		this.y = y;
	}

	public Vector(Point p1, Point p2) {
		this(p2.x() - p1.x(), p2.y() - p1.y());
	}

	public double getMagnitude() {
		if(magnitude != null) {
			return magnitude;
		}

		magnitude = Math.sqrt(x * x + y * y);
		return magnitude;
	}

	public double getRads() {
		if(radians != null) {
			return radians;
		}

		radians = Math.atan2(y, x);
		if(radians < 0) {
			radians += MathUtils.PI2;
		}

		return radians;
	}

	public double dot(Vector v) {
		return x * v.x + y * v.y;
	}
	
	public double cross(Vector v) {
		return x * v.y - y * v.x;
	}

	public Vector multiply(double value) {
		return new Vector(x * value, y * value);
	}

	public double radsBetween(Vector v) {
		return Math.acos(dot(v) / (getMagnitude() * v.getMagnitude()));
	}

	public Vector unit() {
		var magnitude = getMagnitude();
		return new Vector(x / magnitude, y / magnitude);
	}

	@Override
	public int hashCode() {
		return Objects.hash(x, y);
	}

	@Override
	public boolean equals(Object obj) {
		if(this == obj)
			return true;
		if(obj == null)
			return false;
		if(getClass() != obj.getClass())
			return false;
		Vector other = (Vector) obj;
		return Double.doubleToLongBits(x) == Double.doubleToLongBits(other.x) && Double.doubleToLongBits(y) == Double.doubleToLongBits(other.y);
	}

	@Override
	public String toString() {
		return "Vector [x=" + x + ", y=" + y + "]";
	}
}
