package maxflux.math;

public final class MathUtils {
	public static final double EPSILON = 0.0001;
	public static final double PI2 = Math.PI * 2;

	public static boolean fuzzyEquals(double a, double b) {
		var diff = a - b;
		return -EPSILON <= diff && diff <= EPSILON;
	}
}
