package maxflux.math;

import maxflux.math.int32.Intshape;

public sealed interface Shape permits Point, Triangle {
	boolean intersects(Shape shape);

	Intshape round();
}
