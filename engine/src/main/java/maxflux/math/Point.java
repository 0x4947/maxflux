package maxflux.math;

import maxflux.math.int32.Intpoint;

public record Point(double x, double y) implements Shape {
	@Override
	public boolean intersects(Shape shape) {
		return switch(shape) {
			case Point(var x, var y) -> {
				yield MathUtils.fuzzyEquals(this.x, x) && MathUtils.fuzzyEquals(this.y, y);
			}
			case Triangle t -> t.intersects(this);
		};
	}

	public Point translate(Vector v) {
		var x = this.x + v.x;
		var y = this.y + v.y;
		return new Point(x, y);
	}

	public double distanceTo(Point p) {
		var xDiff = p.x() - this.x;
		var yDiff = p.y() - this.y;
		return Math.sqrt(xDiff * xDiff + yDiff * yDiff);
	}

	@Override
	public Intpoint round() {
		var xInt = (int) Math.round(x);
		var yInt = (int) Math.round(y);
		return new Intpoint(xInt, yInt);
	}
}
