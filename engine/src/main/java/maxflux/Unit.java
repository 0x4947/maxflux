package maxflux;

import maxflux.math.Point;
import maxflux.math.Vector;
import maxflux.nav.Path;
import maxflux.nav.Pathfinder;

public class Unit {
	private double speed;
	private Point position;
	private Point destination;
	private Pathfinder pathfinder;
	private Path path;

	/**
	 * @param position
	 * @param speed    - some value per second
	 */
	public Unit(Point position, double speed, Pathfinder pathfinder) {
		this.position = position;
		this.speed = speed;
		this.pathfinder = pathfinder;
	}

	public Point getPosition() {
		return position;
	}
	
	public Pathfinder getPathfinder() {
		return pathfinder;
	}
	
	public Path getPath() {
		return path;
	}

	void setDestination(Point destination) {
		this.destination = destination;
		this.path = null;
	}

	void run(long millis) {
		move(millis);
	}

	private void move(long millis) {
		if(destination == null || speed == 0) {
			return;
		}
		
		if(path == null) {
			path = pathfinder.findShortestRoute(this, destination);
		}

		var stepsPerSec = 1000.0 / millis;
		var stepDistance = speed / stepsPerSec;
		while(!path.waypoints().isEmpty() && stepDistance > 0) {
			var waypoint = path.waypoints().peek();
			var diffX = waypoint.x() - position.x();
			var diffY = waypoint.y() - position.y();
			var targetVector = new Vector(diffX, diffY);
			var targetDistance = targetVector.getMagnitude();
			var unitVector = targetVector.unit();
			var ministepDistance = stepDistance;
			if(targetDistance <= stepDistance) {
				ministepDistance = targetDistance;
				path.waypoints().remove();
			}
			
			var ministepVector = unitVector.multiply(ministepDistance);
			position = position.translate(ministepVector);
			stepDistance -= ministepDistance;
		}
		
		if(path.waypoints().isEmpty()) {
			destination = null;
			path = null;
		}
	}
}
