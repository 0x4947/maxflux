package maxflux.nav;

import java.util.Queue;

import maxflux.math.Point;

public interface Path {
	Queue<Point> waypoints();
}
