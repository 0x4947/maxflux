package maxflux.nav;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Set;

import maxflux.Unit;
import maxflux.math.Point;
import maxflux.math.Vector;

public class AstarSmoothPathfinder implements Pathfinder {
	public Navmesh navmesh;

	public AstarSmoothPathfinder(Navmesh navmesh) {
		this.navmesh = navmesh;
	}

	@Override
	public AstarSmoothPath findShortestRoute(Unit unit, Point destination) {
		Navtriangle originNavtriangle = null;
		Point origin = unit.getPosition();
		for(Navtriangle navtriangle : navmesh.navtriangles) {
			if(navtriangle.triangle.intersects(origin)) {
				originNavtriangle = navtriangle;
				break;
			}
		}

		if(originNavtriangle == null) {
			String errorMsg = String.format("Unit %s is not contained by navmesh.", unit);
			throw new RuntimeException(errorMsg);
		}

		var waypoints = Collections.asLifoQueue(new LinkedList<Point>());
		if(originNavtriangle.triangle.intersects(destination)) {
			waypoints.add(destination);
			return new AstarSmoothPath(waypoints, Collections.emptyList());
		}

		var navnodeInfoMap = new HashMap<Navnode, Info>();
		var fringe = new PriorityQueue<Fringe>();
		fringe.add(new Fringe(originNavtriangle.node1, origin, destination, navnodeInfoMap));
		fringe.add(new Fringe(originNavtriangle.node2, origin, destination, navnodeInfoMap));
		fringe.add(new Fringe(originNavtriangle.node3, origin, destination, navnodeInfoMap));

		var checkedNavtriangles = new HashSet<Navtriangle>();
		Fringe cheapestFringe;
		Navtriangle destNavtriangle = null;
		while((cheapestFringe = fringe.poll()) != null) {
			destNavtriangle = cheapestFringe.findDestNavtriangle(destination, checkedNavtriangles);
			if(destNavtriangle != null) {
				break;
			}

			for(Navnode.Neighbor neighbor : cheapestFringe.navnode.neighbors) {
				var g = cheapestFringe.info.g + neighbor.distance;
				var info = navnodeInfoMap.get(neighbor.node);
				if(info == null) {
					info = new Info(cheapestFringe, neighbor.node, g, destination);
					navnodeInfoMap.put(neighbor.node, info);
					fringe.add(new Fringe(neighbor.node, info));
				}else if(g < info.g) {
					info.setPrev(cheapestFringe.navnode, g);
					readdFringeNode(fringe, neighbor.node, info);
				}
			}
		}

		if(cheapestFringe == null) {
			return new AstarSmoothPath(waypoints, Collections.emptyList());
		}

		var navportals = new LinkedList<Navportal>();
		var tracedNavnodes = new HashSet<Navnode>();
		var waypointNavnode = cheapestFringe.navnode;
		Navnode candidateNavnode1;
		Navnode candidateNavnode2;
		if(cheapestFringe.navnode == destNavtriangle.node1) {
			candidateNavnode1 = destNavtriangle.node2;
			candidateNavnode2 = destNavtriangle.node3;
		}else if(cheapestFringe.navnode == destNavtriangle.node2) {
			candidateNavnode1 = destNavtriangle.node1;
			candidateNavnode2 = destNavtriangle.node3;
		}else {
			candidateNavnode1 = destNavtriangle.node1;
			candidateNavnode2 = destNavtriangle.node2;
		}
		var prevWaypointNavnode = cheapestFringe.info.prev;
		exploreFunnelPassages(
				waypointNavnode,
				candidateNavnode1,
				candidateNavnode2,
				prevWaypointNavnode,
				tracedNavnodes,
				originNavtriangle,
				destination,
				navportals
		);
		Navportal lastNavportal;
		while(!originNavtriangle.has(lastNavportal = navportals.getLast())) {
			waypointNavnode = navnodeInfoMap.get(waypointNavnode).prev;
			candidateNavnode1 = lastNavportal.waypointNavnode();
			candidateNavnode2 = lastNavportal.otherNavnode();
			prevWaypointNavnode = navnodeInfoMap.get(waypointNavnode).prev;
			exploreFunnelPassages(
					waypointNavnode,
					candidateNavnode1,
					candidateNavnode2,
					prevWaypointNavnode,
					tracedNavnodes,
					originNavtriangle,
					destination,
					navportals
			);
		}

		pullString(origin, destination, navportals, waypoints);

		return new AstarSmoothPath(waypoints, navportals);
	}

	private void exploreFunnelPassages(
			Navnode waypointNavnode,
			Navnode candidateNavnode1,
			Navnode candidateNavnode2,
			Navnode prevWaypointNavnode,
			HashSet<Navnode> tracedNavnodes,
			Navtriangle originNavtriangle,
			Point destination,
			LinkedList<Navportal> navportals
	) {
		tracedNavnodes.add(candidateNavnode2);
		var isPassable = exploreFanoutFunnelPassage(
				waypointNavnode,
				candidateNavnode1,
				prevWaypointNavnode,
				tracedNavnodes,
				originNavtriangle,
				destination,
				navportals
		);
		if(!isPassable) {
			tracedNavnodes.remove(candidateNavnode2);
			tracedNavnodes.add(candidateNavnode1);
			exploreFanoutFunnelPassage(
					waypointNavnode,
					candidateNavnode2,
					prevWaypointNavnode,
					tracedNavnodes,
					originNavtriangle,
					destination,
					navportals
			);
		}
	}

	private boolean exploreFanoutFunnelPassage(
			Navnode waypointNavnode,
			Navnode candidateNavnode,
			Navnode prevWaypointNavnode,
			HashSet<Navnode> tracedNavnodes,
			Navtriangle originNavtriangle,
			Point destination,
			LinkedList<Navportal> navportals
	) {
		boolean isWaypointToTheLeft;
		var lastNavportal = navportals.peekLast();
		if(lastNavportal == null) {
			var v1 = new Vector(destination, waypointNavnode.point);
			var v2 = new Vector(destination, candidateNavnode.point);
			var crossProduct = v1.cross(v2);
			isWaypointToTheLeft = crossProduct <= 0;
		}else if(lastNavportal.waypointNavnode() == candidateNavnode) {
			isWaypointToTheLeft = !lastNavportal.isWaypointToTheLeft();
		}else {
			isWaypointToTheLeft = lastNavportal.isWaypointToTheLeft();
		}

		if(prevWaypointNavnode == null && originNavtriangle.has(candidateNavnode)) {
			navportals.add(new Navportal(waypointNavnode, candidateNavnode, isWaypointToTheLeft));
			return true;
		}else if(candidateNavnode == prevWaypointNavnode) {
			return true;
		}

		var commonNeighbors = waypointNavnode.getCommonNeighbors(candidateNavnode);
		commonNeighbors.removeAll(tracedNavnodes);
		var commonNeighborCount = commonNeighbors.size();
		if(commonNeighborCount == 1) {
			tracedNavnodes.add(candidateNavnode);
			navportals.add(new Navportal(waypointNavnode, candidateNavnode, isWaypointToTheLeft));
			var nextCandidateNavnode = commonNeighbors.iterator().next();
			var isPassable = exploreFanoutFunnelPassage(
					waypointNavnode,
					nextCandidateNavnode,
					prevWaypointNavnode,
					tracedNavnodes,
					originNavtriangle,
					destination,
					navportals
			);
			if(!isPassable) {
				tracedNavnodes.remove(candidateNavnode);
				navportals.removeLast();
			}
			return isPassable;
		}else if(commonNeighborCount == 0) {
			// dead end.
			return false;
		}else {
			var message = String.format(
					"UNEXPECTED CASE: %s and %s have more than 1 untraced common neighbor in a funnel.",
					waypointNavnode,
					candidateNavnode
			);
			throw new RuntimeException(message);
		}
	}

	/**
	 * PriorityQueue doesn't track mutations: it is not going to check fringe.info.f
	 * changes, and thus it won't update the order of fringes.
	 */
	private static void readdFringeNode(PriorityQueue<Fringe> fringe, Navnode navnode, Info info) {
		for(Iterator<Fringe> iterator = fringe.iterator(); iterator.hasNext();) {
			Fringe nextFringe = iterator.next();
			if(nextFringe.navnode == navnode) {
				iterator.remove();
				fringe.add(nextFringe);
				return;
			}
		}
	}

	private void pullString(Point origin, Point destination, List<Navportal> navportals, Queue<Point> waypoints) {
		var leftPointQueue = new LinkedList<FunnelSidePoint>();
		var rightPointQueue = new LinkedList<FunnelSidePoint>();
		Navnode lastLeftNavnode = null;
		Navnode lastRightNavnode= null;
		int waypointIndex = 0;
		for(Navportal navportal : navportals) {
			var waypointNavnode = navportal.waypointNavnode();
			
			var leftNavnode = navportal.leftNavnode();
			if(leftNavnode != lastLeftNavnode) {
				var currentWaypointIndex = leftNavnode == waypointNavnode ? ++waypointIndex : -1;
				var funnelSidePoint = new FunnelSidePoint(leftNavnode.point, currentWaypointIndex);
				leftPointQueue.add(funnelSidePoint);
				lastLeftNavnode = leftNavnode;
			}

			var rightNavnode = navportal.rightNavnode();
			if(rightNavnode != lastRightNavnode) {
				var currentWaypointIndex = rightNavnode == waypointNavnode ? ++waypointIndex : -1;
				var funnelSidePoint = new FunnelSidePoint(rightNavnode.point, currentWaypointIndex);
				rightPointQueue.add(funnelSidePoint);
				lastRightNavnode = rightNavnode;
			}
		}
		var originPoint = new FunnelSidePoint(origin, ++waypointIndex);
		leftPointQueue.add(originPoint);
		rightPointQueue.add(originPoint);

		var apex = destination;
		waypoints.add(destination);
		FunnelSidePoint leftPoint = null;
		FunnelSidePoint rightPoint = null;
		outer:while(!leftPointQueue.isEmpty() || !rightPointQueue.isEmpty()) {
			leftPoint = leftPointQueue.peek() != null ? leftPointQueue.poll() : leftPoint;
			rightPoint = rightPointQueue.peek() != null ? rightPointQueue.poll() : rightPoint;
			var leftVector = new Vector(apex, leftPoint.point);
			var rightVector = new Vector(apex, rightPoint.point);
			inner:while(!leftPointQueue.isEmpty() || !rightPointQueue.isEmpty()) {
				var nextLeftPoint = leftPointQueue.peek() != null ? leftPointQueue.peek() : leftPoint;
				var nextLeftVector = new Vector(apex, nextLeftPoint.point);
				boolean isLeftContracting = leftVector.cross(nextLeftVector) <= 0;
				if(isLeftContracting) {
					leftPoint = nextLeftPoint;
					if(rightVector.cross(nextLeftVector) < 0) {
						waypoints.add(rightPoint.point);
						apex = rightPoint.point;
						break inner;
					}else {
						leftPointQueue.poll();
						leftVector = nextLeftVector;
					}
				}else if(rightPoint.equals(originPoint)) {
					break outer;
				}

				var nextRightPoint = rightPointQueue.peek() != null ? rightPointQueue.peek() : rightPoint;
				var nextRightVector = new Vector(apex, nextRightPoint.point);
				boolean isRightContracting = rightVector.cross(nextRightVector) >= 0;
				if(isRightContracting) {
					rightPoint = nextRightPoint;
					if(leftVector.cross(nextRightVector) > 0) {
						waypoints.add(leftPoint.point);
						apex = leftPoint.point;
						break inner;
					}else {
						rightPointQueue.poll();
						rightVector = nextRightVector;
					}
				}else if(leftPoint.equals(originPoint)) {
					break outer;
				}else if(!isLeftContracting) {
					if(leftPoint.waypointIndex > rightPoint.waypointIndex) {
						rightPointQueue.poll();
						rightVector = nextRightVector;
					}else {
						leftPointQueue.poll();
						leftVector = nextLeftVector;
					}
				}
			}
		}
	}

	static class Info {
		Navnode prev;
		double g;
		final double h;
		double f;

		Info(Point origin, Navnode navnode, Point destination) {
			g = origin.distanceTo(navnode.point);
			h = navnode.point.distanceTo(destination);
			f = g + h;
		}

		Info(Fringe fringe, Navnode navnode, double g, Point destination) {
			this.prev = fringe.navnode;
			this.g = g;
			this.h = navnode.point.distanceTo(destination);
			this.f = g + h;
		}

		void setPrev(Navnode prev, double g) {
			this.prev = prev;
			this.g = g;
			this.f = g + h;
		}
	}

	static class Fringe implements Comparable<Fringe> {
		final Navnode navnode;
		final Info info;

		Fringe(Navnode navnode, Info info) {
			this.navnode = navnode;
			this.info = info;
		}

		Fringe(Navnode navnode, Point origin, Point destination, Map<Navnode, Info> navnodeInfoMap) {
			var info = new Info(origin, navnode, destination);
			navnodeInfoMap.put(navnode, info);
			this.navnode = navnode;
			this.info = info;
		}

		Navtriangle findDestNavtriangle(Point destination, Set<Navtriangle> checkedNavtriangles) {
			for(Navtriangle navtriangle : navnode.navtriangles) {
				if(checkedNavtriangles.contains(navtriangle)) {
					continue;
				}

				if(navtriangle.triangle.intersects(destination)) {
					return navtriangle;
				}

				checkedNavtriangles.add(navtriangle);
			}

			return null;
		}

		@Override
		public int compareTo(Fringe o) {
			return Double.compare(info.f, o.info.f);
		}
	}
	
	record FunnelSidePoint(Point point, int waypointIndex) {
		
	}
}
