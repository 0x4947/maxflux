package maxflux.nav;

import maxflux.math.Triangle;

public class Navtriangle {
	Navnode node1;
	Navnode node2;
	Navnode node3;
	Triangle triangle;

	public Navtriangle(Navnode node1, Navnode node2, Navnode node3) {
		this.node1 = node1;
		this.node2 = node2;
		this.node3 = node3;
		node1.connect(node2);
		node1.connect(node3);
		node2.connect(node3);
		this.triangle = new Triangle(node1.point, node2.point, node3.point);
		node1.navtriangles.add(this);
		node2.navtriangles.add(this);
		node3.navtriangles.add(this);
	}

	boolean has(Navnode navnode) {
		return navnode == node1 || navnode == node2 || navnode == node3;
	}

	boolean has(Navportal navportal) {
		return has(navportal.waypointNavnode()) && has(navportal.otherNavnode());
	}
}
