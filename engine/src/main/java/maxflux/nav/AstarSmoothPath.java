package maxflux.nav;

import java.util.List;
import java.util.Queue;

import maxflux.math.Point;

public record AstarSmoothPath(Queue<Point> waypoints, List<Navportal> navportals) implements Path {

}
