package maxflux.nav;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import maxflux.math.MathUtils;
import maxflux.math.Triangle;
import maxflux.math.Vector;

public class Navmesh {
	List<Navtriangle> navtriangles = new ArrayList<Navtriangle>();

	public Navmesh(Navtriangle navtriangle) {
		navtriangles.add(navtriangle);
	}

	public void add(Navtriangle navtriangle) {
		checkSurroundedness(navtriangle.node1);
		checkSurroundedness(navtriangle.node2);
		checkSurroundedness(navtriangle.node3);
		navtriangles.add(navtriangle);
	}

	private void checkSurroundedness(Navnode navnode) {
		double radiansSum = 0;
		for(Navtriangle navtriangle : navnode.navtriangles) {
			Navnode neighbor1;
			Navnode neighbor2;
			if(navnode == navtriangle.node1) {
				neighbor1 = navtriangle.node2;
				neighbor2 = navtriangle.node3;
			}else if(navnode == navtriangle.node2) {
				neighbor1 = navtriangle.node1;
				neighbor2 = navtriangle.node3;
			}else {
				neighbor1 = navtriangle.node1;
				neighbor2 = navtriangle.node2;
			}

			var v1 = new Vector(navnode.point, neighbor1.point);
			var v2 = new Vector(navnode.point, neighbor2.point);
			radiansSum += v1.radsBetween(v2);
		}

		if(MathUtils.fuzzyEquals(radiansSum, MathUtils.PI2)) {
			var message = String.format("%s is fully surrounded by navtriangles. Each navnode must represent the constrainted edge.", navnode);
			throw new IllegalArgumentException(message);
		}
	}

	public Stream<Triangle> getTriangles() {
		return navtriangles.stream().map(nt -> nt.triangle);
	}
}
