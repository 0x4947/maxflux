package maxflux.nav;

import java.util.LinkedList;
import java.util.Queue;

import maxflux.Unit;
import maxflux.math.Point;

public class DirectPathfinder implements Pathfinder {

	@Override
	public Path findShortestRoute(Unit unit, Point destination) {
		var waypoints = new LinkedList<Point>();
		waypoints.add(destination);
		return new Path() {
			@Override
			public Queue<Point> waypoints() {
				return waypoints;
			}
		};
	}

}
