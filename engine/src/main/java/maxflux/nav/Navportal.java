package maxflux.nav;

public record Navportal(Navnode waypointNavnode, Navnode otherNavnode, boolean isWaypointToTheLeft) {
	public Navnode leftNavnode() {
		return isWaypointToTheLeft ? waypointNavnode : otherNavnode;
	}

	public Navnode rightNavnode() {
		return isWaypointToTheLeft ? otherNavnode : waypointNavnode;
	}
}
