package maxflux.nav;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

import maxflux.math.Point;

public class Navnode {
	public final Point point;
	TreeSet<Neighbor> neighbors = new TreeSet<>();
	ArrayList<Navtriangle> navtriangles = new ArrayList<>();

	// used for optimization purposes to prevent mapping from "Neighbor set" into
	// "Navnode set" at runtime
	private HashSet<Navnode> neighborSet = new HashSet<>();

	public Navnode(Point point) {
		this.point = point;
	}

	void connect(Navnode navnode) {
		var distance = point.distanceTo(navnode.point);
		neighbors.add(new Neighbor(navnode, distance));
		navnode.neighbors.add(new Neighbor(this, distance));

		neighborSet.add(navnode);
		navnode.neighborSet.add(this);
	}

	Set<Navnode> getCommonNeighbors(Navnode navnode) {
		var commonNeighbors = new HashSet<Navnode>(this.neighborSet);
		commonNeighbors.retainAll(navnode.neighborSet);
		return commonNeighbors;
	}

	@Override
	public String toString() {
		return "Navnode [point=" + point + "]";
	}

	static class Neighbor implements Comparable<Neighbor> {
		Navnode node;
		double distance;

		private Neighbor(Navnode node, double distance) {
			this.node = node;
			this.distance = distance;
		}

		@Override
		public int compareTo(Neighbor other) {
			if(node == other.node) {
				return 0;
			}

			var distanceComparison = Double.compare(distance, other.distance);
			if(distanceComparison != 0) {
				return distanceComparison;
			}

			// TreeSet should permit two different adjacent nodes that have same distances
			// from the same Navnode,
			// while ensuring that Comparable::compareTo contract is met.
			var hash1 = System.identityHashCode(node);
			var hash2 = System.identityHashCode(other.node);
			return Integer.compare(hash1, hash2);
		}
	}
}
