package maxflux.nav;

import maxflux.Unit;
import maxflux.math.Point;

public interface Pathfinder {
	Path findShortestRoute(Unit unit, Point destination);
}
