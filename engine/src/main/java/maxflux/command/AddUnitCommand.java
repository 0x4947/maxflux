package maxflux.command;

import maxflux.Unit;

public record AddUnitCommand(Unit unit) implements Command {
}
