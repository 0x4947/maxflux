package maxflux.command;

public sealed interface Command permits AddUnitCommand, MoveUnitCommand {
}
