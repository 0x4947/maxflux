package maxflux.command;

import maxflux.Unit;
import maxflux.math.Point;

public record MoveUnitCommand(Unit unit, Point destination) implements Command {
}
