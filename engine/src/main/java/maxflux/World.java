package maxflux;

import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.CopyOnWriteArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import maxflux.command.AddUnitCommand;
import maxflux.command.Command;
import maxflux.command.MoveUnitCommand;

public class World implements Runnable {
	private static final Logger logger = LoggerFactory.getLogger(World.class);

	private final Queue<Command> commandQueue = new ConcurrentLinkedQueue<>();
	private final ArrayList<Unit> units = new ArrayList<>();
	private final CopyOnWriteArrayList<WorldListener> worldListeners = new CopyOnWriteArrayList<>();
	private volatile boolean isRunning;
	private volatile int stepMillis = 1000 / 60;

	public void queueUpCommand(Command command) {
		commandQueue.add(command);
	}

	public void addWorldListener(WorldListener worldListener) {
		worldListeners.add(worldListener);
	}
	
	public void setStepMillis(int stepMillis) {
		this.stepMillis = stepMillis;
	}
	
	public void stop() {
		isRunning = false;
	}
	
	public List<Unit> getUnits() {
		return units;
	}

	@Override
	public void run() {
		isRunning = true;

		while(isRunning) {
			var execMillis = run(stepMillis);
			var idleMillis = Math.max(0, stepMillis - execMillis);
			try {
				Thread.sleep(idleMillis);
			}catch(InterruptedException e) {
				logger.warn("Interrupted?", e);
				isRunning = false;
			}
		}
		
		logger.info("Done running.");
	}

	public long run(long millis) {
		var startMillis = System.currentTimeMillis();
		while(!commandQueue.isEmpty()) {
			var command = commandQueue.poll();
			handle(command);
		}
		var phase1Millis = System.currentTimeMillis() - startMillis;

		units.forEach(u -> u.run(millis));
		var phase2Millis = System.currentTimeMillis() - startMillis - phase1Millis;

		worldListeners.forEach(wl -> wl.handleRunEvent(this));
		var execMillis = System.currentTimeMillis() - startMillis;
		var phase3Millis = execMillis - phase2Millis - phase1Millis;

		logger.info(
				"Exec in {}/{} millis (phase1 in {}, phase2 in {}, phase3 in {}).",
				execMillis,
				millis,
				phase1Millis,
				phase2Millis,
				phase3Millis
		);
		return execMillis;
	}

	private void handle(Command command) {
		switch(command) {
			case AddUnitCommand(var unit) -> units.add(unit);
			case MoveUnitCommand(var unit, var destination) -> unit.setDestination(destination);
		}
	}
}
