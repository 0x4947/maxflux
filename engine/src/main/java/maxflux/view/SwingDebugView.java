package maxflux.view;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.List;

import javax.swing.JPanel;

import maxflux.World;
import maxflux.math.int32.Intpoint;
import maxflux.math.int32.Inttriangle;
import maxflux.nav.AstarSmoothPathfinder;
import maxflux.nav.AstarSmoothPath;
import maxflux.nav.Navportal;

public class SwingDebugView extends JPanel implements DebugView {
	private static final long serialVersionUID = 1L;

	private volatile Info info;

	public SwingDebugView() {
		setBackground(Color.BLACK);
	}

	@Override
	public void render(World world) {
		var units = world.getUnits();
		if(units.isEmpty()) {
			return;
		}else if(units.size() > 1) {
			throw new RuntimeException("Debug view doesn't support more than 1 unit at the moment.");
		}

		var info = new Info();
		var unit = units.get(0);
		info.position = unit.getPosition().round();
		var path = unit.getPath();
		if(path != null) {
			info.waypoints = path.waypoints().stream().map(wp -> wp.round()).toList();
		}

		if(path instanceof AstarSmoothPath astarSmoothPath) {
			info.navportals = astarSmoothPath.navportals();
		}

		var pathfinder = unit.getPathfinder();
		if(pathfinder instanceof AstarSmoothPathfinder astarPathfinder) {
			info.navmeshTriangles = astarPathfinder.navmesh.getTriangles().map(nt -> nt.round()).toList();
		}

		this.info = info;

		repaint();
	}

	@Override
	public void paint(Graphics g) {
		super.paint(g);

		if(info == null) {
			return;
		}

		var g2d = (Graphics2D) g;
		drawNavmesh(g2d);
		drawNavportals(g2d);
		drawPath(g2d);
		drawUnit(g2d);
	}

	private void drawNavmesh(Graphics2D g) {
		if(info.navmeshTriangles == null) {
			return;
		}

		g.setColor(Color.GREEN);
		info.navmeshTriangles.forEach(nt -> SwingShapeDrawer.draw(g, nt));
	}

	private void drawNavportals(Graphics2D g) {
		if(info.navportals == null) {
			return;
		}

		g.setStroke(new BasicStroke(3));
		info.navportals.forEach(np -> {
			var p1 = np.leftNavnode().point.round();
			var p2 = np.rightNavnode().point.round();
			g.setColor(new Color(255, 255, 0, 130));
			SwingShapeDrawer.drawSegment(g, p1, p2);
			
			var p1y = g.getClipBounds().height - p1.y();
			var p2y = g.getClipBounds().height - p2.y();
			g.setColor(new Color(255, 0, 255));
			g.fillRect(p1.x() - 3, p1y - 3, 6, 6);
			g.setColor(new Color(255, 130, 0));
			g.fillRect(p2.x() - 3, p2y - 3, 6, 6);
		});
		g.setStroke(new BasicStroke(1));
	}

	private void drawPath(Graphics2D g) {
		if(info.waypoints == null) {
			return;
		}

		g.setColor(Color.WHITE);
		var prevWaypoint = info.position;
		for(Intpoint waypoint : info.waypoints) {
			SwingShapeDrawer.drawSegment(g, prevWaypoint, waypoint);
			prevWaypoint = waypoint;
		}
	}

	private void drawUnit(Graphics2D g) {
		g.setColor(Color.RED);
		var x = info.position.x();
		var y = g.getClipBounds().height - info.position.y();
		g.drawOval(x - 5, y - 5, 10, 10);
	}

	private static class Info {
		Intpoint position;
		List<Intpoint> waypoints;
		List<Navportal> navportals;
		List<Inttriangle> navmeshTriangles;
	}
}
