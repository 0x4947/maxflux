package maxflux.view;

import maxflux.World;

public interface DebugView {
	void render(World world);
}
