package maxflux.view;

import java.awt.Graphics;

import maxflux.math.int32.Intpoint;
import maxflux.math.int32.Intshape;
import maxflux.math.int32.Inttriangle;

public class SwingShapeDrawer {
	public static void draw(Graphics g, Intshape intshape) {
		switch(intshape) {
			case Intpoint(var x, var y) -> {
				y = g.getClipBounds().height - y;
				g.drawLine(x, y, x, y);
			}
			case Inttriangle(var p1, var p2, var p3) -> {
				var p1y = g.getClipBounds().height - p1.y();
				var p2y = g.getClipBounds().height - p2.y();
				var p3y = g.getClipBounds().height - p3.y();
				g.drawLine(p1.x(), p1y, p2.x(), p2y);
				g.drawLine(p1.x(), p1y, p3.x(), p3y);
				g.drawLine(p2.x(), p2y, p3.x(), p3y);
			}
		};
	}
	
	// TODO remove after the implementation of the dedicated shape
	public static void drawSegment(Graphics g, Intpoint p1, Intpoint p2) {
		var p1y = g.getClipBounds().height - p1.y();
		var p2y = g.getClipBounds().height - p2.y();
		g.drawLine(p1.x(), p1y, p2.x(), p2y);
	}
}
