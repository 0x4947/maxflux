package maxflux;

@FunctionalInterface
public interface WorldListener {
	void handleRunEvent(World world);
}
