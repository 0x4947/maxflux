package maxflux.playtest;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JFrame;
import javax.swing.WindowConstants;

import maxflux.Unit;
import maxflux.World;
import maxflux.command.AddUnitCommand;
import maxflux.command.MoveUnitCommand;
import maxflux.math.Point;
import maxflux.nav.AstarSmoothPathfinder;
import maxflux.nav.Navmesh;
import maxflux.nav.Navnode;
import maxflux.nav.Navtriangle;
import maxflux.view.SwingDebugView;

public class Playtest {
	public static void main(String[] args) {
		var anode = new Navnode(new Point(1200, 700));
		var bnode = new Navnode(new Point(1200, 600));
		var cnode = new Navnode(new Point(1000, 600));
		var dnode = new Navnode(new Point(1000, 700));
		var enode = new Navnode(new Point(900, 600));
		var fnode = new Navnode(new Point(900, 700));
		var gnode = new Navnode(new Point(500, 600));
		var hnode = new Navnode(new Point(500, 700));
		var inode = new Navnode(new Point(1000, 300));
		var jnode = new Navnode(new Point(900, 300));
		var knode = new Navnode(new Point(800, 550));
		var lnode = new Navnode(new Point(1100, 450));
		var mnode = new Navnode(new Point(700, 300));
		var nnode = new Navnode(new Point(700, 200));
		var onode = new Navnode(new Point(900, 200));
		var pnode = new Navnode(new Point(1000, 200));
		var qnode = new Navnode(new Point(800, 400));
		var rnode = new Navnode(new Point(400, 600));
		var snode = new Navnode(new Point(650, 250));
		var tnode = new Navnode(new Point(1250, 680));
		var unode = new Navnode(new Point(1175, 800));
		var navmesh = new Navmesh(new Navtriangle(anode, bnode, cnode));
		navmesh.add(new Navtriangle(anode, cnode, dnode));
		navmesh.add(new Navtriangle(cnode, dnode, fnode));
		navmesh.add(new Navtriangle(cnode, fnode, enode));
		navmesh.add(new Navtriangle(fnode, enode, gnode));
		navmesh.add(new Navtriangle(fnode, gnode, hnode));
		navmesh.add(new Navtriangle(enode, cnode, inode));
		navmesh.add(new Navtriangle(enode, jnode, inode));
		navmesh.add(new Navtriangle(enode, knode, jnode));
		navmesh.add(new Navtriangle(cnode, lnode, inode));
		navmesh.add(new Navtriangle(jnode, inode, onode));
		navmesh.add(new Navtriangle(pnode, inode, onode));
		navmesh.add(new Navtriangle(jnode, nnode, onode));
		navmesh.add(new Navtriangle(jnode, nnode, mnode));
		navmesh.add(new Navtriangle(qnode, knode, jnode));
		navmesh.add(new Navtriangle(rnode, gnode, hnode));
		navmesh.add(new Navtriangle(snode, mnode, nnode));
		navmesh.add(new Navtriangle(tnode, anode, bnode));
		navmesh.add(new Navtriangle(tnode, anode, unode));
		var pathfinder = new AstarSmoothPathfinder(navmesh);
		
		var unit = new Unit(new Point(875, 575), 50, pathfinder);
		var world = new World();
		world.queueUpCommand(new AddUnitCommand(unit));
		var debugView = new SwingDebugView();
		world.addWorldListener(debugView::render);
		debugView.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				var x = e.getX();
				var y = debugView.getHeight() - e.getY();
				world.queueUpCommand(new MoveUnitCommand(unit, new Point(x, y)));
			}
		});
		
		var jframe = new JFrame("Playtest");
		jframe.add(debugView);
		jframe.setVisible(true);
		jframe.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		
		var worldThread = new Thread(world);
		worldThread.start();
	}
}
